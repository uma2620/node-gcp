const { Storage } = require('@google-cloud/storage');
const { config } = require('../config/config');
const util = require('util');
const { statusCodes, statusMessages } = require('../config/status');

const { format } = util;
const validMime = ['image/jpeg', 'image/png', 'image/jpg']


const path = require('path')
const serviceKey = path.join(__dirname, '../config/gcp-keys.json')


const storage = new Storage({
    projectId: config.google.projectId,
    keyFilename: serviceKey,
});

const bucket = storage.bucket(config.google.bucket);



module.exports = {
    uploadImage: async (request, response, next) => {
        try {
            const { originalname, buffer } = request.file;
            if (validMime.includes(request.file.mimetype)) {
                const blob = bucket.file(originalname.replace(/ /g, "_"))
                const stream = blob.createWriteStream({
                    resumable: true,
                    predefinedAcl: 'publicRead',
                });
                stream.on('error', err => {
                    console.log(err)
                    next(err);
                });

                stream.on('finish', () => {
                    response.status(statusCodes.created).json({
                        data: {
                            message: statusMessages.upload
                        },
                    });
                });

                stream.end(request.file.buffer);
            }
            else {
                response.status(statusCodes.bad).json({
                    error: {
                        message: statusMessages.invalidMime
                    },
                });
            }

        }
        catch (error) {
            next(error)
        }
    },
    deleteImage: async (request, response, next) => {
        try {
            const imagename = request.params.image;
            await bucket.file(imagename).delete();
            response.status(statusCodes.success).json({
                data: {
                    message: statusMessages.delete,
                },
            });
        }
        catch (error) {
            response.status(error.code).json({
                data: {
                    message: error.errors[0].message
                }
            })
        }

    },
    getImages: async (request, response, next) => {

        try {
            const [files] = await bucket.getFiles();
            var result = [];

            files.forEach((file) => {
                result.push({ "name": file.name, "size": file.metadata.size, "date": file.metadata.timeCreated });
            });
            response.status(statusCodes.success).json({
                data: result
            });
        }
        catch (error) {
            next(error)
        }

    }
}