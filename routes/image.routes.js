const express = require('express');
const router = express.Router();
const imageController=require('../controllers/image.controller');

router.post('/upload',imageController.uploadImage);
router.get('/list',imageController.getImages);
router.delete('/delete/:image',imageController.deleteImage);


module.exports=router;