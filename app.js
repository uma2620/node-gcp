const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const imageRouter=require('./routes/image.routes');
const multer = require('multer')
const cors = require(`cors`)
const {config}=require('./config/config');

app.all('*', function(req, res, next) {
  var origin = req.get('origin'); 
  res.header('Access-Control-Allow-Origin', origin);
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use(cors());
const multerMid = multer({

  storage: multer.memoryStorage(),

  limits: {

    // no larger than 5mb.

    fileSize: 5 * 1024 * 1024,

  },

});



app.disable('x-powered-by')

app.use(multerMid.single('file'))

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({extended: false}))


app.use('/api/images',imageRouter)

app.use((err, req, res, next) => {

  res.status(500).json({

    data:{error: err,

    message: 'Internal server error!'}

  })

  next()

})




app.listen(config.server.port, () => {
  console.log('Node with GCP!!!')

})