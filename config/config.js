const config= {
	env: process.env.NODE_ENV || 'development',
	server: {
		port: process.env.PORT || 4001,
	},
	google: {
		projectId: 'node-js-287610',
		bucket: 'gcp-node',
	},
};

module.exports={config}