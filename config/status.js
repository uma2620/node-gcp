const statusCodes = {
    success: 200,
    error: 500,
    notfound: 404,
    unauthorized: 401,
    conflict: 409,
    created: 201,
    bad: 400,
    nocontent: 204,
};

const statusMessages={
    upload:"image uploaded sucessfully",
    invalidMime:"Invalid Mimetype(upload jpg/png images only)",
    delete:"Image Deleted Sucessfully"
}
module.exports = {
    statusCodes,statusMessages
}